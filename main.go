package main

import (
	"log/slog"
	"net/http"
	"os"
	"os/exec"
)

func main() {
	addr := os.Getenv("LISTEN_ADDR")
	if addr == "" {
		addr = ":8080"
	}

	fortune, err := exec.LookPath("fortune")
	if err != nil {
		slog.Error("failed to find `fortune`", "err", err)
		os.Exit(1)
	}
	slog.Info("found `fortune`", "path", fortune)

	http.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	http.HandleFunc("/fortune", func(w http.ResponseWriter, r *http.Request) {
		cmd := exec.Command(fortune)
		out, err := cmd.Output()
		if err != nil {
			slog.Error("failed to run `fortune`", "err", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Write(out)
	})

	slog.Info("listening", "addr", addr)
	http.ListenAndServe(addr, nil)
}
