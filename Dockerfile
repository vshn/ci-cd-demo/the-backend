FROM docker.io/library/golang:1.21-alpine AS builder
WORKDIR /src
COPY . .
RUN go build -o /the-backend .

FROM docker.io/library/alpine:3.14
EXPOSE 8080
CMD ["/the-backend"]
RUN apk add --no-cache fortune
COPY --from=builder /the-backend /the-backend
